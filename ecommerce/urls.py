from django.urls import path

from . import views

urlpatterns = [
    path('sync', views.sync_products_with_shopify, name='sync_products_with_shopify'),
    path('products/list', views.list_products, name='list_products'),
    path('order/create', views.create_order, name='create_order'),
]
