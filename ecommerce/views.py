import json

import requests
from django.db import IntegrityError
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from ecommerce.models import *


# Create your views here.

def sync_products_with_shopify(request):
    url = "https://mishipaytestdevelopmentemptystore.myshopify.com/admin/api/2021-01/products.json"
    payload = {}
    headers = {
        'Authorization': 'Basic YTM4ZjRhNmE4Y2I3MTNmZTJiZWJkYmYzZGYzMzFmNTQ6MzE4MmRjZDI5ZmY2YzNmNmYyZGQzMjViYTk5YjQyMTY=',
    }
    response = requests.request("GET", url, headers=headers, data=payload)
    print(response.text)
    products = json.loads(response.text)["products"]

    for p in products:
        try:
            product = Product.objects.create(
                id=p["id"],
                title=p["title"],
                body_html=p["body_html"],
                vendor=p["vendor"],
                product_type=p["product_type"],
                created_at=p["created_at"],
                handle=p["handle"],
                updated_at=p["updated_at"],
                published_at=p["published_at"],
                template_suffix=p["template_suffix"],
                status=p["status"],
                published_scope=p["published_scope"],
                tags=p["tags"],
                admin_graphql_api_id=p["admin_graphql_api_id"],
                image=p["image"],
            )
        except IntegrityError:
            product = Product.objects.get(id=p["id"])

        for v in p["variants"]:
            try:
                variant = ProductVariant.objects.create(
                    id=v["id"],
                    product_id=v["product_id"],
                    title=v["title"],
                    price=float(v["price"]),
                    sku=v["sku"],
                    position=v["position"],
                    inventory_policy=v["inventory_policy"],
                    compare_at_price=v["compare_at_price"],
                    fulfillment_service=v["fulfillment_service"],
                    inventory_management=v["inventory_management"],
                    option1=v["option1"],
                    option2=v["option2"],
                    option3=v["option3"],
                    created_at=v["created_at"],
                    updated_at=v["updated_at"],
                    taxable=v["taxable"],
                    barcode=v["barcode"],
                    grams=v["grams"],
                    image_id=v["image_id"],
                    weight=v["weight"],
                    weight_unit=v["weight_unit"],
                    inventory_item_id=v["inventory_item_id"],
                    inventory_quantity=v["inventory_quantity"],
                    old_inventory_quantity=v["old_inventory_quantity"],
                    requires_shipping=v["requires_shipping"],
                    admin_graphql_api_id=v["admin_graphql_api_id"],
                )
                product.variants.add(variant)
            except IntegrityError:
                pass
            product.save()

        for i in p["images"]:
            try:
                p_image = ProductImages.objects.create(image=i)
                product.images.add(p_image)
            except IntegrityError:
                pass
        product.save()

        for o in p["options"]:
            try:
                p_option = ProductOption.objects.create(
                    id=o["id"],
                    product_id=o["product_id"],
                    name=o["name"],
                    position=o["position"],
                    values=o["values"],
                )
                product.options.add(p_option)
            except IntegrityError:
                pass
        product.save()
    print('success')
    return JsonResponse({
        "ok": True
    })


def list_products(request):
    products = []
    for p in Product.objects.all():
        products.append(p.to_dict())
    return JsonResponse({
        "ok": True,
        "data": products
    })


@csrf_exempt
def create_order(request):
    variant = request.POST.get('variant', None)
    quantity = request.POST.get('quantity', None)

    if variant is None or quantity is None:
        return JsonResponse({
            "ok": False,
            "message": "variant id and quantity are required form-date fields"
        }, status=422)

    headers = {
        'Content-Type': 'application/json',
    }
    payload = {
        "order": {
            "line_items": [{
                "variant_id": 32328277590083, "quantity": 2
            }]
        }
    }
    url = 'https://a38f4a6a8cb713fe2bebdbf3df331f54:3182dcd29ff6c3f6f2dd325ba99b4216@mishipaytestdevelopmentemptystore.myshopify.com/admin/api/2021-01/orders.json'
    response = requests.request("POST", url, headers=headers, data=payload)
    print(response.text)
    # order = Order.objects.create()
    return JsonResponse({
        "ok": True
    })
