from django.db import models

from utilities.models import TimeStampMixin


class Product(TimeStampMixin):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=500)
    body_html = models.TextField(blank=True, null=True)
    vendor = models.CharField(max_length=500)
    product_type = models.CharField(max_length=500)
    handle = models.CharField(max_length=500)
    published_at = models.DateTimeField(blank=True, null=True)
    template_suffix = models.CharField(max_length=500, blank=True, null=True)
    status = models.CharField(max_length=10, choices=[('active', 'Active'), ('inactive', 'Inactive')])
    published_scope = models.CharField(max_length=500, blank=True, null=True)
    tags = models.TextField(blank=True, null=True)
    admin_graphql_api_id = models.CharField(max_length=500, blank=True, null=True)
    variants = models.ManyToManyField('ecommerce.ProductVariant')
    options = models.ManyToManyField('ecommerce.ProductOption')
    images = models.ManyToManyField('ecommerce.ProductImages')
    image = models.CharField(max_length=1000, null=True, blank=True)

    def to_dict(self):
        d = {
            "id": self.id,
            "title": self.title,
            "body_html": self.body_html,
            "vendor": self.vendor,
            "product_type": self.product_type,
            "created_at": self.created_at,
            "handle": self.handle,
            "updated_at": self.updated_at,
            "published_at": self.published_at,
            "template_suffix": self.template_suffix,
            "status": self.status,
            "published_scope": self.published_scope,
            "tags": self.tags,
            "admin_graphql_api_id": self.admin_graphql_api_id,
            "image": self.image,
            "variants": [],
            "options": [],
            "images": [],
        }
        for item in self.variants.all():
            d["variants"].append(item.to_dict())
        for item in self.options.all():
            d["options"].append(item.to_dict())
        for item in self.images.all():
            d["images"].append(item.image)
        return d


class ProductOption(TimeStampMixin):
    id = models.IntegerField(primary_key=True)
    product_id = models.CharField(max_length=500)
    name = models.CharField(max_length=500)
    position = models.PositiveIntegerField()
    values = models.TextField(help_text='a string array in JSON')

    def to_dict(self):
        return {
            "id": self.id,
            "product_id": self.product_id,
            "name": self.name,
            "position": self.position,
            "values": self.values,
        }


class ProductImages(TimeStampMixin):
    image = models.CharField(max_length=1000)


class ProductVariant(TimeStampMixin):
    id = models.IntegerField(primary_key=True)
    product_id = models.CharField(max_length=500)
    title = models.CharField(max_length=500)
    price = models.FloatField()
    sku = models.CharField(max_length=500)
    position = models.PositiveIntegerField()
    inventory_policy = models.CharField(max_length=500)
    compare_at_price = models.CharField(max_length=500, null=True, blank=True)
    fulfillment_service = models.CharField(max_length=500)
    inventory_management = models.CharField(max_length=500, null=True, blank=True)
    option1 = models.CharField(max_length=500, null=True, blank=True)
    option2 = models.CharField(max_length=500, null=True, blank=True)
    option3 = models.CharField(max_length=500, null=True, blank=True)
    taxable = models.BooleanField(default=True)
    barcode = models.CharField(max_length=500, blank=True, null=True)
    grams = models.PositiveIntegerField()
    image_id = models.CharField(max_length=500, blank=True, null=True)
    weight = models.PositiveIntegerField()
    weight_unit = models.CharField(max_length=10, choices=[('kg', 'kilogrames'), ('g', 'grams')])
    inventory_item_id = models.PositiveIntegerField()
    inventory_quantity = models.PositiveIntegerField()
    old_inventory_quantity = models.PositiveIntegerField()
    requires_shipping = models.BooleanField(default=True)
    admin_graphql_api_id = models.CharField(max_length=500, blank=True, null=True)

    def to_dict(self):
        return {
            "id": self.id,
            "product_id": self.product_id,
            "title": self.title,
            "price": self.price,
            "sku": self.sku,
            "position": self.position,
            "inventory_policy": self.inventory_policy,
            "compare_at_price": self.compare_at_price,
            "fulfillment_service": self.fulfillment_service,
            "inventory_management": self.inventory_management,
            "option1": self.option1,
            "option2": self.option2,
            "option3": self.option3,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "taxable": self.taxable,
            "barcode": self.barcode,
            "grams": self.grams,
            "image_id": self.image_id,
            "weight": self.weight,
            "weight_unit": self.weight_unit,
            "inventory_item_id": self.inventory_item_id,
            "inventory_quantity": self.inventory_quantity,
            "old_inventory_quantity": self.old_inventory_quantity,
            "requires_shipping": self.requires_shipping,
            "admin_graphql_api_id": self.admin_graphql_api_id,
        }
