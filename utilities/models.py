from django.db import models


class TimeStampMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, help_text="UST only")
    updated_at = models.DateTimeField(auto_now=True, help_text="UST only")

    class Meta:
        abstract = True


class AddressMixin(models.Model):
    lat = models.CharField(max_length=20, blank=True, null=True)
    lon = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        abstract = True
