# A simple eCommerce backend developed on Django to interact with Shopify APIs

## created by: Aishwary Dhare

## Instructions to setup this project

### Step 1 : Setting up project

- create a virtualenv with python3.7 named 'venv': [refer](https://www.geeksforgeeks.org/python-virtual-environment/)
- activate virtualenv: `source venv/bin/activate`
- install dependencies in virtualenv from req.txt file: `pip install -r req.txt`
- Init model migrations: `python manage.py migrate`

### Step 2 : Setting up scheduled job and first db sync

- open cron jobs editor: `crontab -e`
- add this line at the bottom: `0 */1 * * * /path/to/project/directory/venv/bin/python manage.py shell < ecommerce/sync-with-shopify.py` (make sure you enter the proper path)
- note that `0 */1 * * *` basically instructs cron to execute this command at minute 0 past every hour

### Step 3: Trying APIs

- find the `mishipay-ecommerce-app.postman_collection.json` in this same git repo
- import it in your postman
- make sure the virtualenv is activated  
- run the app using `python manage.py runserver` to bootup server on [http://localhost:8000](http://localhost:8000)

### Step 4: Validate

- create superuser credentials using `python manage.py createsuperuser`
- open [http://localhost:8000/admin](http://localhost:8000/admin) and login with credentials created
- validate orders and products by checking data
